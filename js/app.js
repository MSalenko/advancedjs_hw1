// 1. Прототипне наслідування в JavaScript - це механізм, за допомогою якого об'єкт може успадковувати властивості
// та методи іншого об'єкта, який використовується як його прототип;
// 2. Ключове слово super використовується для виклику конструктора батьківського класу в класі-нащадку.
// Коли клас успадковує властивості та методи від іншого класу, його конструктор повинен також викликати 
// конструктор батьківського класу, щоб забезпечити коректну ініціалізацію об'єкта.

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(newName) {
    this._name = newName;
  }
  get age() {
    return this._age;
  }
  set age(newAge) {
    this._age = newAge;
  }
  get salary() {
    return this._salary;
  }
  set salary(newSalary) {
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
  set salary(newSalary) {
    this._salary = newSalary;
  }
  get lang() {
    return this._lang;
  }
  set lang(newLang) {
    if (!Array.isArray(newLang)) {
      return;
    }
    this._lang = newLang;
  }
}

const developer1 = new Programmer('Max', 42, 2000, ['English', 'Ukrainian']);
const developer2 = new Programmer('John', 25, 1000, ['English']);
const developer3 = new Programmer('Valerie', 28, 2500, ['English', 'French', 'Italian']);

console.log(developer1);
console.log(developer2);
console.log(developer3);